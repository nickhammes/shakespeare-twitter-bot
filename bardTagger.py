#!/usr/bin/env python
import nltk
import os
import cPickle
import sys

def shakespeareTagger():
#	shakespeare = cPickle.load(open("shakespeare_naive_tagging_mk4.cpickle", 'r'))
	shakespeare = naiveShakespeareTagger()
	shakespeare = switchTagger(shakespeare)
	pickleSentences(shakespeare, "shakespeare_switch_tagging_mk4.cpickle")
def naiveShakespeareTagger():
	williamsWords = open("shakespeare_100_lines.txt")
	sentences = list()
	i = 0
	for line in williamsWords:
		x = nltk.pos_tag(nltk.word_tokenize(line))
		sentences.append(x)
		print i
		i += 1
	return sentences
def pickleSentences(sentences, filename):
	file = open(filename,"a")
	cPickle.dump(sentences, file)
	file.flush()
	file.close()
	return;
def writeSentences(sentences, filename):
	taggedShakespeare = open(filename, "a")
	taggedShakespeare.writelines("%s\n" % item for item in sentences)
	taggedShakespeare.flush()
	taggedShakespeare.close()

def switchTagger(taggedSents):
#Takes a naively-tagged list of sentences
	sentences = list()
	for line in taggedSents:
		for (word, tag) in line:
			if (word == "methinks"):
				tag = "VBZ"
	return sentences

shakespeareTagger()