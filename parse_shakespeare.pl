#!/usr/bin/perl
use strict;
use warnings;

my $filename = "shakespeare_full_works.txt";
open( my $shakespeare, '<', $filename ) or die "Can't open $filename: $!";
while ( my $line = <$shakespeare> ) {
  if ( $line =~ /^(\s|\d)*$/ ) {
    # Blank lines
  }
  elsif ( $line =~ /^\d*$/ ) {
    # Numbers in Sonnets
  }
  elsif ( $line !~ /[a-z]/ ) {
    # Metadata, Scene names
  }
  elsif ( $line =~ /^\s*Enter\s*/ ){
    # Enter NAME lines
  }
  elsif ( $line =~ /^\s*Alarum.\s*/ ){
    # Different Enter NAME lines
  }
  elsif ( $line =~ /^\s*Flourish\s*/ ){
    # Different Enter NAME line still
  }
  elsif ( $line =~ /\:\/\// ){
    # Lines with URLs are garbage
  }
  elsif ( $line =~ /Re-enter.*/ ) {
    # Lines where characters re-enter (uncommon)
  }
  elsif ( $line =~ /by William Shakespeare/ ) {
    # Silly line that doesn't belong, but doesn't fit any pattern.
  }
  # parse lines i want to keep.
  else { 
    $line =~ s/^[A-Z][A-Z ]+[\.\,]//g; #Throw out speakers' names.
    $line =~ s/\s*Exeunt.*$//g; #Throw out certain exit lines.
    $line =~ s/\[.*\]//g; #Throw out stage directives.
    $line =~ s/^\s*//g; #Throw out leading whitespace.
    $line =~ s/\s*Exit.*//g; #Throw out exit directives.
    $line =~ s/\s*\[.*//g;  #Throw out multi-line stage directives' beginnings.
    $line =~ s/.*\]//g;  #Throw out multi-line stage directives' endings.
    $line =~ s/[^\S\n]+$//g; #Throw out trailing whitespace.
    if ( $line =~ /[Ia-z,'-;:]\n/ ) { #Does this line end with the end of a sentence?
      $line =~ s/\n$/ /g; #Replace the end-of-line character with a space.
    }
	$line =~ s/^[A-Z]+(\ )[A-Z]+\.//g; #Ensure speakers' names with two parts are gone.
	$line =~ s/^[A-Z]+\.//g; #Ensure speakers' names with one part are gone.
	$line =~ s/[\.]\s/\.\n/g; #Force multi-sentence lines to split, if ending with ".".
	$line =~ s/[\!]\s/\!\n/g; #Force multi-sentence lines to split, if ending with "!".
	$line =~ s/[\?]\s/\?\n/g; #Force multi-sentence lines to split, if ending with "?".
	$line =~ s/^\s*//g; #Ensure leading whitespace is gone.
    print $line;
  }
}
