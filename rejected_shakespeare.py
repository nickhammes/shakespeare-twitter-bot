import twitter
import string
import sys, re
import nltk
import cPickle
import random
import time
from nltk import wordpunct_tokenize
def main():
  credentials = open("twitter_creds", "r")
  creds = credentials.readlines()
  for x in range(0,len(creds)):
    creds[x] = chomp(creds[x])
  William = twitter.Api(consumer_key=creds[0], consumer_secret=creds[1], access_token_key=creds[2], access_token_secret=creds[3])
  tweetReply(William)
  tweet = "this is my 140 character witty line"
  #Author Tweet Here
  tweet = compose_tweet(tweet)
  tweet = re.sub(" ,",",",tweet)
  tweet = re.sub(" \.",".",tweet)
  tweet = re.sub(" ;",";",tweet)
  tweet = re.sub(" :",":",tweet)
  tweet = re.sub(" !","!",tweet)
  tweet = re.sub(' \?','?',tweet)
  tweet = re.sub("givn","giv'n",tweet)
  tweet = re.sub("wll","'ll",tweet)
  tweet = re.sub("zs","'s",tweet)
  tweet = re.sub("zt","'t",tweet)
  tweet = re.sub("manzs","man's",tweet)
  tweet = re.sub("serpentzs","serpent's",tweet)
  tweet = re.sub(r" '","'",tweet)
  #Finish Tweet-authoring lines
  print tweet
  post = raw_input("Post tweet? ")
  if(post == "Y"):
    post_tweet(tweet, William)
	
def insultingReply(mention):
	print mention
	passmention = re.sub("@RejectedWillS ", "", mention)
	templating = 0
	correctFile = ""
	if(isHateful(passmention) != 0):
		correctFile = "insults1"
	elif(isInsultable(passmention) != 0):
		correctFile = "insults2"
	elif(isPraiseworthy(passmention) != 0):
		correctFile = "praises"
	else:
		correctFile = "insults3"
		templating = 1
	if (templating == 0):
		f = open(correctFile, 'rU')
		lines = f.readlines()
		w = random.randint(0,len(lines) - 1)
		tweet = lines[w]
	else:
		f1 = open("insults3-1", "rU")
		f2 = open("insults3-2", "rU")
		f3 = open("insults3-3", "rU")
		tweet = "Thou"
		lines = list()
		lines.append(f1.readlines())
		lines.append(f2.readlines())
		lines.append(f3.readlines())
		for line in lines:
			w = random.randint(0,len(line) - 1)
			tweet += " " + line[w]
			tweet = re.sub("\n","",tweet)
		tweet += "!"
	tweet = re.sub("/","\n",tweet)
	return (tweet, correctFile)
	
def isInsultable(tweet):
	f = open("intelligence_insultable", "rU")
	lines = f.readlines()
	for pattern in lines:
		pattern = re.sub("\n","",pattern)
		pattern = re.sub("\\s","\s",pattern)
		if(re.search(pattern,tweet)):
			print pattern
			return 1
	if((len(tweet)/(len(wordpunct_tokenize(tweet)))) < 4):
		return 2
	return 0

def isHateful(tweet):
	f = open("hatespeech.txt", "rU")
	lines = f.readlines()
	for pattern in lines:
		pattern = re.sub("\n","",pattern)
		pattern = re.sub("\\s","\s",pattern)
		if(re.search(pattern,tweet)):
			print pattern
			return 1
	if(re.search("gay\s",tweet) and re.search("dude",tweet)):
		print "reason 2"
		return 2
	return 0
	
def isPraiseworthy(tweet):
	f = open("praisables.txt", "rU")
	lines = f.readlines()
	for pattern in lines:
		pattern = re.sub("\n","",pattern)
		pattern = re.sub("\\s","\s",pattern)
		if(re.search(pattern,tweet)):
			return 1
	return 0

def compose_tweet(tweet):
  f = open('sents.txt', 'rU')
  lines = f.readlines()
  sentences = []
  index = 0
  while lines:
    sentences.append(sentence())
    sentences[index].readLower(lines.pop())
    sentences[index].readUpper(lines.pop())
    index += 1
  #Pick a sentence template to use
  sentindex = random.randint(0,len(sentences) - 1)
  #print sentindex
  shakespeare = cPickle.load(open("shakespeare_naive_tagging_mk4.cpickle", 'r'))
  sentences[sentindex].fillUpper(shakespeare)
  tweet = " ".join(sentences[sentindex].upper)
  if (len(tweet) > 140):
    print "warning: tweet longer than 140 characters.\n"
  return tweet

def post_tweet(tweet, William):
  status = William.PostUpdate(tweet)	

def tweetReply(William):
	#Get old and current mention lists
	mentions = William.GetReplies()
	#print "len(set(mentions)) =", len(set(mentions))
	file = open("mentions.txt", "r")
	previousMentions = cPickle.load(file)
	#print "len(set(previousMentions)) =", len(set(previousMentions))
	mentionIDs = set([t.created_at_in_seconds for t in mentions])
	toReplyToIDs = mentionIDs - set(previousMentions)
	file.flush()
	file.close()
	#Generate list of mentions to act on
	replyQueue = list()
	for m in mentions:
		for ID in toReplyToIDs:
			if (m.created_at_in_seconds == ID):
				print ID
				replyQueue.append(m)
				break
	#print replyQueue
	#Process replyQueue to tweets
	if (len(replyQueue) == 0):
		#print "No new tweets to respond to."
		return
	else:
		while (len(replyQueue) > 0):
			(tweet, filename) = insultingReply(replyQueue[0].text)
			tweet = "@" + str(replyQueue[0].user.screen_name) + " " + tweet
			print "Responded:", tweet, filename
			if len(tweet) > 140:
				print "LENGTH GREATER THAN 140! OOPS!"
			reply = twitter.Status()
			reply.text = tweet
			reply.SetUser(William.GetUser("RejectedWillS"))
			reply.in_reply_to_screen_name = replyQueue[0].user.screen_name
			reply.in_reply_to_user_id = replyQueue[0].user.id
			reply.in_reply_to_status_id = replyQueue[0].id
			William.PostUpdate(reply.text, replyQueue[0].in_reply_to_status_id)
			print reply.in_reply_to_status_id
			previousMentions.append(replyQueue[0].created_at_in_seconds)
			replyQueue.pop(0)
		print "len(previousMentions) =", len(previousMentions)
		newfile = open("mentions.txt", "w")
		cPickle.dump(previousMentions, newfile)
		newfile.flush()
		newfile.close()
	
class sentence:
	upper = []
	lower = []
	def readUpper(self, sent):
		self.upper = wordpunct_tokenize(sent)
	def readLower(self, sent):
		self.lower = wordpunct_tokenize(sent)
	def fillUpper(self, shakespeare):
	  ZZSET = ""
	  VVSET = ""
	  for word in range(0,len(self.upper) - 1):
	    if(self.upper[word] == "XX") or (self.upper[word] == "ZZ") or (self.upper[word] == "VV"):
	      w = random.randint(0,tagCount(shakespeare,self.lower[word])) #choose word
	      #print "w = ", w
	      for sent in shakespeare:
	        for (W,T) in sent:
	          if(T == self.lower[word]):
	            if(w == 0):
	              if(self.upper[word] == "ZZ"):
	                ZZSET = W
	              elif(self.upper[word] == "VV"):
	                VVSET = W
	              self.upper[word] = W
	              w -= 1
	              break
	              break
	            else:
	              w -= 1
	    elif(self.upper[word] == "YY"):
	      self.upper[word] = ZZSET
	    elif(self.upper[word] == "WW"):
	      self.upper[word] == VVSET

def tagCount(shakespeare, tag):
  n = 0
  for sent in shakespeare:
    for (w,t) in sent:
      if (tag == t): 
        n += 1
  return n
def chomp(s):
    if s.endswith('\n'):
        return s[:-1]
    else:
        return s
#Execute
main()